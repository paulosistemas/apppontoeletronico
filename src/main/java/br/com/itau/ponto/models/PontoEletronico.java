package br.com.itau.ponto.models;

import br.com.itau.ponto.enums.TipoPontoEletronico;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
public class PontoEletronico {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int idPonto;

    private LocalDateTime dataHoraRegistro;

    public void setDataHoraRegistro(LocalDateTime dataHoraRegistro) {
        this.dataHoraRegistro = dataHoraRegistro;
    }

    public LocalDateTime getDataHoraRegistro() {
        return dataHoraRegistro;
    }

    private TipoPontoEletronico tipoPontoEletronico;

    @ManyToOne
    @JoinColumn(name = "id_Usuario", nullable = false)
    private Usuario usuario;

    public int getIdPonto() {
        return idPonto;
    }

    public void setIdPonto(int idPonto) {
        this.idPonto = idPonto;
    }

    public TipoPontoEletronico getTipoPontoEletronico() {
        return tipoPontoEletronico;
    }

    public void setTipoPontoEletronico(TipoPontoEletronico tipoPontoEletronico) {
        this.tipoPontoEletronico = tipoPontoEletronico;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
