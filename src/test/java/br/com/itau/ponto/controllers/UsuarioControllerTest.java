package br.com.itau.ponto.controllers;

import br.com.itau.ponto.DTOs.RespostaPontoEletronicoDTO;
import br.com.itau.ponto.enums.TipoPontoEletronico;
import br.com.itau.ponto.models.PontoEletronico;
import br.com.itau.ponto.models.Usuario;
import br.com.itau.ponto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    Usuario usuario;

    @BeforeEach
    public void setUp() {

        usuario = new Usuario();
        usuario.setCpf("011.291.201-10");
        usuario.setDataCadastro(LocalDate.now());
        usuario.setNome("Batman");
        usuario.setIdUsuario(1);

    }

    @Test
    public void testarBuscasTodosUsuario() throws Exception {

        Iterable<Usuario> usuarioIterable = Arrays.asList(usuario);

        Mockito.when(usuarioService.listarTodosUsuarios()).thenReturn(usuarioIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

    @Test
    public void testarBuscasTodosUsuarioSemRetorno() throws Exception {

        Iterable<Usuario> usuarioIterable = new ArrayList<>();

        Mockito.when(usuarioService.listarTodosUsuarios()).thenReturn(usuarioIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios")
                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isBadRequest());
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(0));
    }

    @Test
    public void testarBuscarUsuario() throws Exception {

        Mockito.when(usuarioService.buscarUsuarioPorId(anyInt())).thenReturn(usuario);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/" + usuario.getIdUsuario())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testarBuscarUsuarioSemRetorno() throws Exception {

        int idUsuario = 1;
        Mockito.when(usuarioService.buscarUsuarioPorId(anyInt())).then(e -> {
            throw new RuntimeException("Usuario não encontrado, ID Informado:" + idUsuario);
        });

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/" + idUsuario)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testarCriarUsuarioComErro() throws Exception{

        String dadosIncorretos = "";
        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(dadosIncorretos))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarCriarUsuario() throws Exception{

        usuario = new Usuario();
        usuario.setCpf("011.291.201-10");
        usuario.setNome("Batman");
        usuario.setIdUsuario(1);

        Mockito.when(usuarioService.salvarUsuario(Mockito.any(Usuario.class)))
                .then(leadObjeto -> {
                     return usuario;
                });
        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }
}
//    @Test
//    public void testarCriarUsuario{
//
//        usuarioService.salvarUsuario(usuario)
//
//}