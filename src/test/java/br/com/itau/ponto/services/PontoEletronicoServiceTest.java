package br.com.itau.ponto.services;

import br.com.itau.ponto.enums.TipoPontoEletronico;
import br.com.itau.ponto.models.PontoEletronico;
import br.com.itau.ponto.models.Usuario;
import br.com.itau.ponto.repositories.PontoEletronicoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

@SpringBootTest
public class PontoEletronicoServiceTest {

    @MockBean
    private PontoEletronicoRepository pontoEletronicoRepository;

    @Autowired
    private PontoEletronicoService pontoEletronicoService;

    ArrayList<PontoEletronico> pontoEletronicos;
    PontoEletronico pontoEntrada;
    PontoEletronico pontoSaida;
    Usuario usuario;

    @BeforeEach
    public void setUp() {

        usuario = new Usuario();
        usuario.setCpf("011.291.201-10");
        usuario.setDataCadastro(LocalDate.now());
        usuario.setIdUsuario(1);
        usuario.setNome("Capitão Futuro");

        pontoEntrada = new PontoEletronico();
        pontoEntrada.setUsuario(usuario);
        pontoEntrada.setIdPonto(1);
        pontoEntrada.setDataHoraRegistro(LocalDateTime.parse("2020-07-02T08:30:00.000"));
        pontoEntrada.setTipoPontoEletronico(TipoPontoEletronico.ENTRADA);

        pontoSaida = new PontoEletronico();
        pontoSaida.setUsuario(usuario);
        pontoSaida.setIdPonto(2);
        pontoSaida.setDataHoraRegistro(LocalDateTime.parse("2020-07-02T16:29:00.000"));
        pontoSaida.setTipoPontoEletronico(TipoPontoEletronico.SAIDA);

    }

    @Test
    public void testarHorasTotais() {
        pontoEletronicos = new ArrayList<PontoEletronico>();
        pontoEletronicos.add(pontoEntrada);
        pontoEletronicos.add(pontoSaida);
        Iterable<PontoEletronico> pontoEletronicoIterable = pontoEletronicos;

        Mockito.when(pontoEletronicoRepository.findAllByUsuario(usuario)).thenReturn(pontoEletronicos);

        PeriodoService periodoService = pontoEletronicoService.horasTotais(usuario);

        Assertions.assertEquals(7,periodoService.getHorasPeriodo());
        Assertions.assertEquals(59,periodoService.getMinutosPeriodo());
    }

    @Test
    public void testarHorasTotaisSemRetorno() {

        pontoEletronicos = new ArrayList<PontoEletronico>();
        Iterable<PontoEletronico> pontoEletronicoIterable = pontoEletronicos;

        Mockito.when(pontoEletronicoRepository.findAllByUsuario(usuario)).thenReturn(pontoEletronicos);

        Assertions.assertThrows(RuntimeException.class, () -> {this.pontoEletronicoService.horasTotais(usuario);});
    }

    @Test
    public void testarRegistrarPontoEletronico(){

        PontoEletronico pontoEletronico = new PontoEletronico();
        pontoEletronico.setTipoPontoEletronico(TipoPontoEletronico.ENTRADA);
        pontoEletronico.setDataHoraRegistro(LocalDateTime.now());

        Mockito.when(pontoEletronicoService.registrarPontoEletronico(pontoEletronico, 1))
                .thenReturn(pontoEletronico);

        Assertions.assertEquals(pontoEletronico, pontoEletronicoService.registrarPontoEletronico(pontoEletronico, 1));

    }
}