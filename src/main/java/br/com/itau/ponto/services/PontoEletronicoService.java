package br.com.itau.ponto.services;

import br.com.itau.ponto.enums.TipoPontoEletronico;
import br.com.itau.ponto.models.PontoEletronico;
import br.com.itau.ponto.models.Usuario;
import br.com.itau.ponto.repositories.PontoEletronicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

@Service
public class PontoEletronicoService {

    @Autowired
    private PontoEletronicoRepository pontoEletronicoRepository;

    @Autowired
    private UsuarioService usuarioService;

    public PontoEletronico registrarPontoEletronico(PontoEletronico pontoEletronico, int idUsuario){

        pontoEletronico.setUsuario(usuarioService.buscarUsuarioPorId(idUsuario));
        PontoEletronico pontoEletronicoObjeto = pontoEletronicoRepository.save(pontoEletronico);

        return pontoEletronicoObjeto;
    }


    public Iterable<PontoEletronico> listaTodosPontos(Usuario usuario) throws RuntimeException{

        Iterable<PontoEletronico> pontoEletronicoIterable = pontoEletronicoRepository.findAllByUsuario(usuario);

        int qtdePontos = 0;
        for (PontoEletronico pontoEletronico : pontoEletronicoIterable) {
            qtdePontos++;
        }
            
        if (qtdePontos>0) {
            return pontoEletronicoIterable;
        }else {
            throw new RuntimeException("Usuario " + usuario.getNome() + " não possui marcações de ponto");
        }
    }

    public PeriodoService horasTotais(Usuario usuario) throws RuntimeException{

        LocalDateTime horarioInicial = LocalDateTime.now();
        LocalDateTime horarioFinal;

        long minutos = 0;

        Iterable <PontoEletronico> listaPontoEletronico = listaTodosPontos(usuario);

        int qtdeRegistros = 0;

        for (PontoEletronico pontoEletronico:listaPontoEletronico) {

            if (pontoEletronico.getTipoPontoEletronico()== TipoPontoEletronico.ENTRADA){

                horarioInicial = pontoEletronico.getDataHoraRegistro();

            }else{

                horarioFinal = pontoEletronico.getDataHoraRegistro();
                minutos += ChronoUnit.MINUTES.between(horarioInicial, horarioFinal);

            }

            qtdeRegistros++;
        }

        if (qtdeRegistros > 0) {
            return new PeriodoService((minutos / 60), (minutos % 60));
        }else{
            throw new RuntimeException("Usuario " + usuario.getNome() + " não possui marcações de ponto");
        }

    }
}