package br.com.itau.ponto.controllers;

import br.com.itau.ponto.DTOs.RespostaPontoEletronicoDTO;
import br.com.itau.ponto.models.PontoEletronico;
import br.com.itau.ponto.models.Usuario;
import br.com.itau.ponto.services.PeriodoService;
import br.com.itau.ponto.services.PontoEletronicoService;
import br.com.itau.ponto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/usuarios/{idUsuario}/pontos")
public class PontoEletronicoController {

    @Autowired
    private PontoEletronicoService pontoEletronicoService;

    @Autowired
    private UsuarioService usuarioService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PontoEletronico registrarPonto(@RequestBody PontoEletronico pontoEletronico,
                                          @PathVariable(name = "idUsuario") int idUsuario){
        PontoEletronico pontoEletronicoObjeto = pontoEletronicoService
                                                              .registrarPontoEletronico(pontoEletronico, idUsuario);
        return pontoEletronicoObjeto;
    }

    @GetMapping
    public RespostaPontoEletronicoDTO buscarTodosPontos(@PathVariable(name = "idUsuario") int idUsuario){

        Usuario usuario;

        try{
            usuario = usuarioService.buscarUsuarioPorId(idUsuario);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

        PeriodoService periodoService;

        try {
            periodoService = pontoEletronicoService.horasTotais(usuario);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

        RespostaPontoEletronicoDTO respostaPontoEletronicoDTO = new RespostaPontoEletronicoDTO();
        respostaPontoEletronicoDTO.setHorasTrabalhadas(periodoService.getHorasPeriodo());
        respostaPontoEletronicoDTO.setMinutosTrabalhados(periodoService.getMinutosPeriodo());
        try {
            respostaPontoEletronicoDTO.setPontoEletronicoServices(pontoEletronicoService.listaTodosPontos(usuario));
            return respostaPontoEletronicoDTO;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

    }
}