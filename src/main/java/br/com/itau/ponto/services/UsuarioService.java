package br.com.itau.ponto.services;

import br.com.itau.ponto.models.Usuario;
import br.com.itau.ponto.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario salvarUsuario(Usuario usuario){
        Usuario usuarioObjeto = usuarioRepository.save(usuario);
        return usuarioObjeto;
    }

    public Usuario buscarUsuarioPorId(int idUsuario){

        Optional <Usuario> optionalUsuario = usuarioRepository.findById(idUsuario);

        if (optionalUsuario.isPresent()){
            return optionalUsuario.get();
        }

        throw new RuntimeException(("Usuario não encontrado, ID Informado:" + idUsuario));
    }

    public Iterable<Usuario> listarTodosUsuarios(){
        return usuarioRepository.findAll();
    }
}