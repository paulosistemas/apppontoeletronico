package br.com.itau.ponto.DTOs;

import br.com.itau.ponto.models.PontoEletronico;

public class RespostaPontoEletronicoDTO {
    private Iterable<PontoEletronico> pontoEletronico;
    private long horasTrabalhadas;
    private long minutosTrabalhados;

    public RespostaPontoEletronicoDTO() {

    }

    public Iterable<PontoEletronico> getPontoEletronico() {
        return pontoEletronico;
    }

    public void setPontoEletronicoServices(Iterable<PontoEletronico> pontoEletronico) {
        this.pontoEletronico = pontoEletronico;
    }

    public long getHorasTrabalhadas() {
        return horasTrabalhadas;
    }

    public void setHorasTrabalhadas(long horasTrabalhadas) {
        this.horasTrabalhadas = horasTrabalhadas;
    }

    public long getMinutosTrabalhados() {
        return minutosTrabalhados;
    }

    public void setMinutosTrabalhados(long minutosTrabalhados) {
        this.minutosTrabalhados = minutosTrabalhados;
    }
}
