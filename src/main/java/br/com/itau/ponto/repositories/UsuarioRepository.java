package br.com.itau.ponto.repositories;

import br.com.itau.ponto.models.PontoEletronico;
import br.com.itau.ponto.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {
}
