package br.com.itau.ponto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppPontoEletronico {

    public static void main(String[] args) {
        SpringApplication.run(AppPontoEletronico.class, args);
    }

}
