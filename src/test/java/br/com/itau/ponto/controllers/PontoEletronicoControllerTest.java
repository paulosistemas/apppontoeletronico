package br.com.itau.ponto.controllers;

import br.com.itau.ponto.DTOs.RespostaPontoEletronicoDTO;
import br.com.itau.ponto.enums.TipoPontoEletronico;
import br.com.itau.ponto.models.PontoEletronico;
import br.com.itau.ponto.models.Usuario;
import br.com.itau.ponto.services.PeriodoService;
import br.com.itau.ponto.services.PontoEletronicoService;
import br.com.itau.ponto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PontoEletronicoController.class)
public class PontoEletronicoControllerTest {

    @MockBean
    private PontoEletronicoService pontoEletronicoService;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    @Autowired
    private UsuarioService usuarioService;

    @MockBean
    PeriodoService periodoService;

    Usuario usuario;

    RespostaPontoEletronicoDTO respostaPontoEletronicoDTO;

    @BeforeEach
    public void setUp(){

        usuario = new Usuario();
        usuario.setCpf("011.291.201-10");
        usuario.setDataCadastro(LocalDate.now());
        usuario.setIdUsuario(1);
        usuario.setNome("Homem de Ferro");

    }

    @Test
    public void testarBuscarTodosPontos() throws Exception{

        periodoService = new PeriodoService(10, 11);
        int idUsuario = 1;

        Mockito.when(usuarioService.buscarUsuarioPorId(Mockito.anyInt())).thenReturn(usuario);
        Mockito.when(pontoEletronicoService.horasTotais(Mockito.any(Usuario.class))).thenReturn(periodoService);

        mockMvc.perform(get("/usuarios/" + idUsuario + "/pontos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testarBuscarTodosPontosRetornoSemRegistro() throws Exception{

        periodoService = new PeriodoService();

        int idUsuario = 3;

        Mockito.when(usuarioService.buscarUsuarioPorId(Mockito.anyInt())).thenReturn(usuario);


        Mockito.when(pontoEletronicoService.horasTotais(Mockito.any(Usuario.class))).then( e -> {
            throw new RuntimeException("Usuario " + usuario.getNome() + " não possui marcações de ponto");
        });

        mockMvc.perform(get("/usuarios/" + idUsuario + "/pontos")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void testarRegistrarPontoDadosIncorretos() throws Exception {

        PontoEletronico pontoEletronico = new PontoEletronico();
        int idUsuario = 1;
        pontoEletronico.setUsuario(usuario);
        pontoEletronico.setIdPonto(1);
        pontoEletronico.setDataHoraRegistro(LocalDateTime.now());
        pontoEletronico.setTipoPontoEletronico(TipoPontoEletronico.ENTRADA);

        Mockito.when(pontoEletronicoService.registrarPontoEletronico(Mockito.any(PontoEletronico.class), Mockito.anyInt()))
                .then(leadObjeto -> {
                    pontoEletronico.setUsuario(usuario);
                    pontoEletronico.setIdPonto(1);
                    pontoEletronico.setTipoPontoEletronico(TipoPontoEletronico.ENTRADA);
            return pontoEletronico;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonPontoEletronico = mapper.writeValueAsString(pontoEletronico);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios/" + idUsuario + "/pontos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonPontoEletronico))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    @Test
    public void testarRegistrarPonto() throws Exception {

        usuario = new Usuario();
        usuario.setCpf("011.291.201-10");
        usuario.setIdUsuario(1);
        usuario.setNome("Homem de Ferro");

        PontoEletronico pontoEletronico = new PontoEletronico();
        int idUsuario = 1;
        pontoEletronico.setUsuario(usuario);
        pontoEletronico.setIdPonto(1);
        pontoEletronico.setTipoPontoEletronico(TipoPontoEletronico.ENTRADA);

        Mockito.when(pontoEletronicoService.registrarPontoEletronico(Mockito.any(PontoEletronico.class), Mockito.anyInt()))
                .then(leadObjeto -> {
                    pontoEletronico.setUsuario(usuario);
                    pontoEletronico.setIdPonto(1);
                    pontoEletronico.setTipoPontoEletronico(TipoPontoEletronico.ENTRADA);
                    return pontoEletronico;
                });

        ObjectMapper mapper = new ObjectMapper();
        String jsonPontoEletronico = mapper.writeValueAsString(pontoEletronico);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios/" + idUsuario + "/pontos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonPontoEletronico))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }
}