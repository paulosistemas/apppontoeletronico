**POST /usuario**
Cadastra usuário com os dados informados

Request Body
{
    "nome": "Mulher Maravilha",
    "cpf": "295.255.068-94",
    "dataCadastro": "2020-07-05"
}

Response 201
{
    "idUsuario": 2,
    "nome": "Mulher Maravilha",
    "cpf": "295.255.068-94",
    "dataCadastro": "2020-07-05"
}

**GET /usuarios/{idUsuario}**
Exibe detalhes do usuário informado

Response 200
{
    "idUsuario": 1,
    "nome": "Capitão América",
    "cpf": "294.731.508-14",
    "dataCadastro": "2020-07-04"
}

**GET /usuarios/**
Lista os detalhes de todos os usuários cadastrados
Response 200
[
    {
        "idUsuario": 1,
        "nome": "Capitão América",
        "cpf": "294.731.508-14",
        "dataCadastro": "2020-07-04"
    },
    {
        "idUsuario": 2,
        "nome": "Mulher Maravilha",
        "cpf": "295.255.068-94",
        "dataCadastro": "2020-07-05"
    }
]

**POST /usuarios/{idUsuario}/pontos**
Registra ponto de usuário, pode ser do tipo entrada ou saída.

Request Body
{
    "dataHoraRegistro": "2020-07-05T20:15:27.001",    
    "tipoPontoEletronico": "ENTRADA"
}

**GET /usuarios/{idUsuario}/pontos**
Exibe todos os registros de ponto do Usuário informado e retorna o acumulado de horas e minutos.

Response 200
{
    "pontoEletronico": [
        {
            "idPonto": 5,
            "dataHoraRegistro": "2020-07-05T08:16:27",
            "tipoPontoEletronico": "ENTRADA",
            "usuario": {
                "idUsuario": 1,
                "nome": "Capitão América",
                "cpf": "294.731.508-14",
                "dataCadastro": "2020-07-04"
            }
        },
        {
            "idPonto": 6,
            "dataHoraRegistro": "2020-07-05T20:15:27",
            "tipoPontoEletronico": "SAIDA",
            "usuario": {
                "idUsuario": 1,
                "nome": "Capitão América",
                "cpf": "294.731.508-14",
                "dataCadastro": "2020-07-04"
            }
        }
    ],
    "horasTrabalhadas": 11,
    "minutosTrabalhados": 59
}