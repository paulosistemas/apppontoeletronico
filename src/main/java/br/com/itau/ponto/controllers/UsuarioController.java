package br.com.itau.ponto.controllers;

import br.com.itau.ponto.models.Usuario;
import br.com.itau.ponto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Usuario> criarUsuario(@RequestBody Usuario usuario){

        Usuario usuarioObjeto = usuarioService.salvarUsuario(usuario);

        return ResponseEntity.status(201).body(usuarioObjeto);

    }

    @GetMapping
    public Iterable<Usuario> buscarTodosUsuarios(){

        return usuarioService.listarTodosUsuarios();

    }

    @GetMapping("/{idUsuario}")
    public Usuario buscarUsuario(@PathVariable(name = "idUsuario") int idUsuario){

        try{
            return usuarioService.buscarUsuarioPorId(idUsuario);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

    }

}