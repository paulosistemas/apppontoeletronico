package br.com.itau.ponto.services;

import br.com.itau.ponto.enums.TipoPontoEletronico;
import br.com.itau.ponto.models.PontoEletronico;
import br.com.itau.ponto.models.Usuario;
import br.com.itau.ponto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {

    Usuario usuario;

    @Autowired
    public UsuarioService usuarioService;

    @MockBean
    UsuarioRepository usuarioRepository;

    @BeforeEach
    public void setUp() {

//        usuarioService = new UsuarioService();


        usuario = new Usuario();
        usuario.setCpf("011.291.201-10");
        usuario.setDataCadastro(LocalDate.now());
        usuario.setIdUsuario(1);
    }

    @Test
    public void testarBuscarUsuarioPorId(){
        Optional <Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
        Usuario usuarioObjeto = usuarioService.buscarUsuarioPorId(1);
        Assertions.assertEquals(usuarioOptional.get(), usuarioObjeto);
    }

    @Test
    public void testarBuscarUsuarioPorIdSemRetorno(){

        Optional <Usuario> usuarioOptional = Optional.empty();
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
        Assertions.assertThrows(RuntimeException.class, () -> {this.usuarioService.buscarUsuarioPorId(1);});

    }

    @Test
    public void testarSalvarUsuario(){

        Mockito.when(usuarioService.salvarUsuario(usuario))
                .thenReturn(usuario);

        Assertions.assertEquals(usuario, usuarioService.salvarUsuario(usuario));
    }

    @Test
    public void testarlistarTodosUsuarios(){

        ArrayList <Usuario> usuarios = new ArrayList<Usuario>();
        usuarios.add(usuario);
        Iterable<Usuario> usuarioIterable = usuarios;
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarioIterable);

        Assertions.assertEquals(usuarioIterable, usuarioService.listarTodosUsuarios());

    }

    @Test
    public void testarlistarTodosUsuariosRetornoVazio(){

        Iterable<Usuario> usuarioIterable = new ArrayList<Usuario>();
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarioIterable);

        Assertions.assertEquals(usuarioIterable, usuarioService.listarTodosUsuarios());

    }
}