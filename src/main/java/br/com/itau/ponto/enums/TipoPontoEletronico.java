package br.com.itau.ponto.enums;

public enum TipoPontoEletronico {
    ENTRADA,
    SAIDA
}
