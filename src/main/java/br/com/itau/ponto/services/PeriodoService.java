package br.com.itau.ponto.services;

import org.springframework.stereotype.Service;


public class PeriodoService {

    private long horasPeriodo;
    private long minutosPeriodo;

    public PeriodoService(long horasPeriodo, long minutosPeriodo) {

        this.horasPeriodo = horasPeriodo;
        this.minutosPeriodo = minutosPeriodo;

    }
    public PeriodoService() {
    }

    public long getHorasPeriodo() {
        return horasPeriodo;
    }

    public void setHorasPeriodo(long horasPeriodo) {
        this.horasPeriodo = horasPeriodo;
    }

    public long getMinutosPeriodo() {
        return minutosPeriodo;
    }

    public void setMinutosPeriodo(long minutosPeriodo) {
        this.minutosPeriodo = minutosPeriodo;
    }
}
